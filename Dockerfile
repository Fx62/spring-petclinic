FROM maven as BUILDER

WORKDIR /app

COPY app/ .

RUN ./mvnw package

FROM maven

WORKDIR /app

COPY --from=BUILDER /app/target/*.jar .

CMD java -jar /app/*.jar
